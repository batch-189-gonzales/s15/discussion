// [COMMENTS]
// This is a single-line comment for short descriptions

/*
This is a multi-line comment for longer descriptions
and multi-line programming
*/

// for outputting data or text into the browser console
console.log('Hello World!');

// [SYNTAX AND STATEMENTS]
// Syntax is the code that makes up a statement
// console
// log()

// Statements are made up of syntax that ...
// console.log('Hello World!');

// [VARIABLES]
// let variables are variables that CAN be re-assigned
let firstName = 'Listher';
console.log(firstName);

let lastName = 'Gonzales';
console.log(lastName);

// Re-assigning a value to a let variable shows no errors 
firstName = 'Beelzebub';
console.log(firstName);

// const variables are variables that CANNOT be re-assigned
const colorOfTheSun = 'yellow';
console.log('The color of the sun is ' + colorOfTheSun);

//  Error happens when you try to re-assign value of a const variable
// colorOfTheSun = 'red';
// console.log('The color of the sun is ' + colorOfTheSun);

// When declaring a variable, you use a keyword like 'let'
let variableName = 'Value';

// When re-assigning a variable, you just need to call the variable name
variableName = 'New Value';

// [DATA TYPES]
// String - denoted by single OR double quotation marks
let fullName = 'Listher Gonzales';

// Number - no quotation marks
let userAge = 26;

// Boolean - value is either 'true' or 'false'
let hasGirlfriend = false;

// Array - denoted by brackets and can contain multiple values inside
let hobbies = ['Travel', 'Game', 'Watch'];

// Object - denoted by curly braces and has value name/label for each value
let person = {
	fullName: 'Listher Gonzales',
	userAge: 26,
	hasGirlfriend: false,
	hobbies: ['Travel', 'Games', 'Watch', 'Read']
};

// Null - a placeholder for future variable re-assignment
let wallet = null;

// console.log each of the variables
console.log(fullName);
console.log(userAge);
console.log(hasGirlfriend);
console.log(hobbies);
console.log(person);
console.log(wallet);

// to display single value of an object
console.log(person.userAge);

// to display single value of an array
console.log(hobbies[0]);